# Introduction

This project provides a few basic TACACS+ examples for Nokia SRLinux

## SRLinux configuration example
```
# info from running / system aaa
    system {
        aaa {
            authentication {
                authentication-method [
                    tac_plus
                    local
                ]
            }
            accounting {
                accounting-method [
                    tac_plus
                    local
                ]
                event command {
                    record start-stop
                }
            }
            server-group local {
                type local
            }
            server-group tac_plus {
                type tacacs
                timeout 2
                server 192.168.1.1 {
                    network-instance mgmt
                    tacacs {
                        port 49
                        secret-key LetmeInNow 
                    }
                }
            }
        }
    }
```

## Tacacs+ configuration example

```
# cat tac_plus.cfg
#!/usr/local/sbin/tac_plus
id = spawnd {
    listen = { port = 49 }
    spawn = {
        instances min = 1
        instances max = 10
    }
}

id = tac_plus {
    debug = PACKET AUTHEN AUTHOR

    log = stdout {
        destination = /dev/stdout
    }

    authorization log group = yes
    authentication log = /var/log/tac_plus.log
    authorization log = /var/log/tac_plus.log
    accounting log = /var/log/tac_plus.log
    #accounting log = /var/log/tac_plus_acct%Y%m%d.log

    retire limit = 1000
    #mavis module = external {
    #    exec = /usr/local/lib/mavis_tacplus_passwd.pl
    #    # see the MAVIS configuration manual for more options and other modules
    #}
    #login backend = mavis

    host = anyv4 {
        welcome banner = "\nWelcome to the tacacs v4 server login\n\n"
        address = 0.0.0.0/0
        enable = clear enable
        key = LetmeInNow
    }

    host = anyv6 {
        welcome banner = "\nWelcome to the tacacs v6 server login\n\n"
        address = ::/0
        enable = clear enable
        key = LetmeInNow
    }

    group = admin {
        default service = permit
        enable = permit
        service = shell {
            default command = permit
            default attribute = permit
            set priv-lvl = 15
        }
    }

    group = limited {
        default service = permit
        enable = permit
        service = shell {
            default command = permit
            default attribute = permit
            set priv-lvl = 3
        }
    }

    user = admin {
        password = clear admin
        member = admin
    }

    user = limited {
        password = clear limited
        member = limited
    }
}
```

- Start container
```
docker run --name tac_plus -d -p 49:49 -v $(pwd)/tac_plus.cfg:/etc/tac_plus/tac_plus.cfg henderiw/tacacs-plus-alpine:1.0.0
```
- Monitor logs from a hypervisor level
```
docker logs -f tac_plus
```
- Stop container
```
docker rm tac_plus -f
```
